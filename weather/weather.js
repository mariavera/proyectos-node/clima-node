const axios = require('axios');

const getClima = async (lat,lon) => {

    const latLon = `${lat},${lon}`;
    const apiKey = '7ad64a6a0a2241b1af6152002200110';

    const resp =await axios.get( `http://api.weatherapi.com/v1/current.json?key=${apiKey}&q=${latLon}`);

    return resp.data.current.temp_c
    
}

module.exports={
    getClima,
}