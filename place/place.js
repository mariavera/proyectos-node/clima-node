const axios = require('axios');

const getLugar = async (direccion) => {

    const ciudad = encodeURI(direccion);
    console.log('ENCFODE', ciudad);

    const instance = axios.create({
        baseURL: `https://geocode.xyz/Hauptstr.,+57632+"${ciudad}"?json=1`,
    });

   const resp = await instance.get()
   console.log(resp.data);
   if ( resp.data.matches===null){
       
       throw new Error(`No hay resultados para ${direccion}`)
   }
   const data= resp.data;
    const lugar = data.standard.city;
   const lat = data.latt;
   const lng = data.longt
      //matches

    return {
        lugar,
        lat,
        lng
    }

}

module.exports={
    getLugar,
}