//https://rapidapi.com/dev132/api/city-geo-location-lookup
//https://geocode.xyz/Hauptstr.,+57632+"New York"?json=1

//https://home.openweathermap.org/

//https://www.weatherapi.com/docs/ = > funciona

//AXIOS Y REQUEST
//AXIOS => PROMESAS
//REQUEST => CALLBACKS

//APIKEY WEATHER 5fe9751316667d67014b599e6fb99cab
const argv = require('yargs').options({
    direccion: {
        alias: 'd',
        desc: 'Direccion de la ciudad para obtener clima',
        demand: true
    }
}).argv;

const {
    getLugar
} = require('./place/place');
const place = require('./place/place');
const clima = require('./weather/weather')

// place.getLugar(argv.direccion)
// .then(console.log )
// clima.getClima(40.68908,-73.95861)
// .then(console.log)
// .catch(console.log)

const getInfo = async (direccion) => {
    try {
        const coords = await place.getLugar(direccion);
        const temp = await clima.getClima(coords.lat, coords.lng);
        return `El clima de ${coords.lugar} es ${temp} °C`;

    } catch (e) {
        return `No se pudo determinar el clima de ${direccion} `;


    }



}

getInfo(argv.direccion)
.then(console.log)
.catch(console.log)